export class ApiError extends Error {
  constructor (message = 'Api error', payload = {}) {
    super(message)

    this.payload = payload
  }
}

function handleFetchErrors (response) {
  if (! response.ok) {
      throw new ApiError(response.statusText, { status: response.status, text: response.responseText })
  }

  return response
}

function fetchJSON (url) {
  return fetch(url)
    .then(handleFetchErrors)
    .then((response) => response.json())
}

export default class Api {
    baseURL = 'https://auto1-mock-server.herokuapp.com/api'

    getAllCars (params = { manufacturer: null, color: null, page: 1 }) {
      const searchParams = Object.keys(params).reduce(
        (acc, cur) => {
          if (Boolean(params[cur])) {
            acc.set(cur, params[cur])
          }

          return acc
        },
        new URLSearchParams(),
      )

      return fetchJSON(`${this.baseURL}/cars?${searchParams.toString()}`)
    }

    getOneCar (stockNumber) {
      return fetchJSON(`${this.baseURL}/cars/${stockNumber}`)
    }

    getColors () {
      return fetchJSON(`${this.baseURL}/colors`)
    }

    getManufacturers () {
      return fetchJSON(`${this.baseURL}/manufacturers`)
    }
}
