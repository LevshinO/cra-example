import React from 'react'
import { render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { QueryParamProvider } from 'use-query-params'

import Pagination from '../../components/Pagination'


describe('<Pagination />', () => {
  test('should render "First" page link', () => {
    const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

    expect(getByText(/first/i)).toBeInTheDocument()
  })

  test('should render "Previous" page link', () => {
    const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

    expect(getByText(/previous/i)).toBeInTheDocument()
  })

  test('should render "Page 2 of 10" according to props', () => {
    let { getByText } = render(
      <QueryParamProvider>
        <Pagination
          initialPage={1}
          totalItems={100}
        />
      </QueryParamProvider>
    )

    expect(getByText(/page 2 of 10/i)).toBeInTheDocument()
  })

  test('should render "Next" page link', () => {
    const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

    expect(getByText(/next/i)).toBeInTheDocument()
  })

  test('should render "Last" page link', () => {
    const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

    expect(getByText(/last/i)).toBeInTheDocument()
  })

  describe('on first page', () => {
    test('should disable "First" page link', () => {
      const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

      expect(getByText(/first/i)).toHaveAttribute('disabled')
    })

    test('should disable "Previous" page link', () => {
      const { getByText } = render(<Pagination />, { wrapper: QueryParamProvider })

      expect(getByText(/previous/i)).toHaveAttribute('disabled')
    })

    test('should enable "Next" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/next/i)).not.toHaveAttribute('disabled')
    })

    test('should render "Page 2 of 10" according to user action', async () => {
      let { getByText } = render(
        <QueryParamProvider>
          <Pagination
            totalItems={100}
          />
        </QueryParamProvider>
      )

      userEvent.click(getByText(/next/i))

      expect(getByText(/page 2 of 10/i)).toBeInTheDocument()
    })
  })

  describe('on not last page and not first page', () => {
    test('should enable "First" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={1}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/first/i)).not.toHaveAttribute('disabled')
    })

    test('should enable "Previous" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={1}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/previous/i)).not.toHaveAttribute('disabled')
    })

    test('should enable "Next" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={1}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/next/i)).not.toHaveAttribute('disabled')
    })

    test('should enable "Last" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={1}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/next/i)).not.toHaveAttribute('disabled')
    })

    test('should render "Page 10 of 10" according to user action', async () => {
      let { getByText } = render(
        <QueryParamProvider>
          <Pagination
            totalItems={100}
          />
        </QueryParamProvider>
      )

      userEvent.click(getByText(/last/i))

      expect(getByText(/page 10 of 10/i)).toBeInTheDocument()
    })
  })

  describe('on last page', () => {
    test('should disable "Next" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={9}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/next/i)).toHaveAttribute('disabled')
    })

    test('should disable "Last" page link', () => {
      const { getByText } = render(
        <QueryParamProvider>
          <Pagination
            initialPage={9}
            totalItems={100}
          />
        </QueryParamProvider>
      )

      expect(getByText(/last/i)).toHaveAttribute('disabled')
    })
  })
})
