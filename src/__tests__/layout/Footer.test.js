import React from 'react'
import { render } from '@testing-library/react'

import Footer from '../../layout/Footer'


describe('<Footer />', () => {
  test(`should render "© AUTO1 Group ${new Date().getFullYear()}"`, () => {
    const { getByText } = render(<Footer />)

    expect(getByText(`© AUTO1 Group ${new Date().getFullYear()}`)).toBeInTheDocument()
  })
})
