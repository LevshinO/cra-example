import React from 'react'
import { render } from '@testing-library/react'
import { MemoryRouter } from "react-router-dom"

import Header from '../../layout/Header'


describe('<Header />', () => {
  test('should render <Logo />', () => {
    const { getByTestId } = render(<Header />, { wrapper: MemoryRouter })

    expect(getByTestId('logo')).toBeInTheDocument()
  })

  test('should render menu with correct links', () => {
    const { getByText } = render(<Header />, { wrapper: MemoryRouter })

    expect(getByText(/purchase/i)).toHaveAttribute('href', '/purchase')
    expect(getByText(/my orders/i)).toHaveAttribute('href', '/my-orders')
    expect(getByText(/sell/i)).toHaveAttribute('href', '/sell')
  })
})
