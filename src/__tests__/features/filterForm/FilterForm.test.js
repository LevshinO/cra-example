import { promises as fs } from 'fs'
import React from 'react'
import { render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { Provider } from 'react-redux'
import { MemoryRouter, Route } from "react-router-dom"
import { QueryParamProvider } from 'use-query-params'

import defaultStore from '../../../store'

import FilterForm from '../../../features/filterForm/FilterForm'


function renderWithWrappers(
  component,
  {
    store = defaultStore,
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return <Provider store={store}>
      <MemoryRouter>
        <QueryParamProvider ReactRouterRoute={Route}>
          {children}
        </QueryParamProvider>
      </MemoryRouter>
    </Provider>
  }

  return render(component, { wrapper: Wrapper, ...renderOptions })
}

const server = setupServer(
  rest.get('https://auto1-mock-server.herokuapp.com/api/colors', async (req, res, ctx) => {
    const colors = JSON.parse(await fs.readFile(`${__dirname}/colors.json`))
    return res(ctx.json(colors))
  }),
  rest.get('https://auto1-mock-server.herokuapp.com/api/manufacturers', async (req, res, ctx) => {
    const manufacturers = JSON.parse(await fs.readFile(`${__dirname}/manufacturers.json`))
    return res(ctx.json(manufacturers))
  }),
)

beforeAll(() => server.listen({
  onUnhandledRequest: 'warn',
}))
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe('<FilterForm />', () => {
  test('should render "Color" field', () => {
    const { getByLabelText, getByTestId } = renderWithWrappers(<FilterForm />)

    expect(getByLabelText('Color')).toBeInTheDocument()
    expect(getByTestId('filter-color')).toBeInTheDocument()
  })

  test('should render "Manufacturer" field', () => {
    const { getByLabelText, getByTestId } = renderWithWrappers(<FilterForm />)

    expect(getByLabelText('Manufacturer')).toBeInTheDocument()
    expect(getByTestId('filter-manufacturer')).toBeInTheDocument()
  })

  test('should render "Filter" submit button', () => {
    const { getByText } = renderWithWrappers(<FilterForm />)

    expect(getByText('Filter')).toBeInTheDocument()
  })

  test('should change address bar to selected values on "Filter" submit', async () => {
    let testLocation;

    const { findByText, getByTestId, getByText } = renderWithWrappers(
      <>
        <FilterForm />
        <Route
          path="*"
          render={({ location }) => {
            testLocation = location
            return null
          }}
        />
      </>
    )

    expect(await findByText('black')).toBeInTheDocument()
    expect(await findByText('BMW')).toBeInTheDocument()

    userEvent.selectOptions(getByTestId('filter-color'), ['black'])
    userEvent.selectOptions(getByTestId('filter-manufacturer'), ['BMW'])
    userEvent.click(getByText('Filter'))

    expect(testLocation.query.color).toBe('black')
    expect(testLocation.query.manufacturer).toBe('BMW')

    userEvent.selectOptions(getByTestId('filter-color'), ['black'])
    userEvent.selectOptions(getByTestId('filter-manufacturer'), [''])
    userEvent.click(getByText('Filter'))

    expect(testLocation.query.color).toBe('black')
    expect(testLocation.query.manufacturer).toBe('')

    userEvent.selectOptions(getByTestId('filter-color'), [''])
    userEvent.selectOptions(getByTestId('filter-manufacturer'), ['BMW'])
    userEvent.click(getByText('Filter'))

    expect(testLocation.query.color).toBe('')
    expect(testLocation.query.manufacturer).toBe('BMW')

    userEvent.selectOptions(getByTestId('filter-color'), [''])
    userEvent.selectOptions(getByTestId('filter-manufacturer'), [''])
    userEvent.click(getByText('Filter'))

    expect(testLocation.query.color).toBe('')
    expect(testLocation.query.manufacturer).toBe('')
  })
})
