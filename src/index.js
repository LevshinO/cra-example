import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom"
import { QueryParamProvider } from 'use-query-params'

import App from './features/app/App'
import store from './store'

import GlobalStyle from './globalStyles'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <GlobalStyle />

      <Router>
        <QueryParamProvider ReactRouterRoute={Route}>
          <App />
        </QueryParamProvider>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)
