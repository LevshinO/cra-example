import React from 'react'
import styled from 'styled-components'

import { Styles } from '../utils/constants'


const FooterStyled = styled.footer`
  align-items: center;
  text-align: center;
  border: 1px solid ${Styles.Colors.LightGray};
  display: flex;
  height: 80px;
  padding: 24px;
`

const Footer = () => (
  <FooterStyled>
    <p style={{ flexBasis: '100%', textAlign: 'center' }}>
      © AUTO1 Group { new Date().getFullYear() }
    </p>
  </FooterStyled>
)

export default Footer
