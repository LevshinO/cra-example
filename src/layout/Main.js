import styled from 'styled-components'


const MainStyled = styled.main`
  display: flex;
  flex: 1 0 auto;
  margin-bottom: 24px;
  padding: 0 24px;
  width: '100%';
`

export default MainStyled
