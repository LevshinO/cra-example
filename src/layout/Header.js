import React from 'react'
import styled from 'styled-components'

import Logo from '../components/Logo'
import Link from '../components/Link'

import { Styles, MenuLinks } from '../utils/constants'


const MenuLink = styled(Link)`
  color: ${Styles.Colors.DarkGray};
  margin-right: 24px;

  &:hover {
    color: ${Styles.Colors.DarkGray};
  }

  &:last-child {
    margin-right: 0;
  }
`

const Menu = () => (
  <nav>
    {
      MenuLinks.map(({ name, href }, idx) => (
        <MenuLink key={`menuItem-${idx}`} to={href}>
          {name}
        </MenuLink>
      ))
    }
  </nav>
)

const HeaderStyled = styled.header`
  align-items: center;
  background: #fff;
  border: 1px solid ${Styles.Colors.LightGray};
  display: flex;
  height: 80px;
  justify-content: space-between;
  margin-bottom: 24px;
  padding: 24px;
  position: sticky;
  top: 0;
  z-index: 100;
`

const Header = () => (
  <HeaderStyled>
    <Logo />
    <Menu />
  </HeaderStyled>
)

export default Header
