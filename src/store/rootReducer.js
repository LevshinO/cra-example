import { combineReducers } from '@reduxjs/toolkit'

import filterForm from '../features/filterForm/slice'
import itemsList from '../features/itemsList/slice'
import itemView from '../features/itemView/slice'

const rootReducer = combineReducers({
  filterForm,
  itemsList,
  itemView,
})

export default rootReducer
