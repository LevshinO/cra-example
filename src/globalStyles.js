import { createGlobalStyle } from 'styled-components';

import { Styles } from './utils/constants'


const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
  }

  html {
    height: 100%;
  }

  body {
    color: ${Styles.Colors.DarkGray};
    font: 14px Roboto;
    min-height: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
  }
`;

export default GlobalStyle;
