import React, { useEffect } from 'react'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { useParams, useHistory } from 'react-router-dom'
import styled from 'styled-components'

import useLocalStorage from '../../hooks/useLocalStorage'

import Button from '../../components/Button'
import { Form, FormItem, FormFooter } from '../../components/Form'

import {
  defaultSelector as itemViewSelector,
  fetchItem,
} from './slice'

import { Styles } from '../../utils/constants'


const ItemViewWrapper = styled.div`
  width: 100%;
`

const ItemViewImage = styled.div`
  background-color: ${Styles.Colors.LightGray};
  margin: -24px -24px 24px;
  padding-bottom: 25%;
`

const ItemViewContainer = styled.article`
  display: flex;
  margin: auto;
  width: 800px;
`

const ItemViewContent = styled.div`
  padding-right: 24px;
  flex-basis: 60%;
`

const ItemViewSidebar = styled.aside`
  flex-basis: 40%;
`

const ItemView = () => {
  const { stockNumber } = useParams()
  const history = useHistory()

  const dispatch = useDispatch()

  useEffect(() => {
    (async () => {
      const error = await dispatch(fetchItem(stockNumber))

      if (error?.payload?.status === 404) {
        history.replace('/error/404')
      }
    })()
  }, [dispatch, history, stockNumber])

  const { item } = useSelector(
    itemViewSelector,
    shallowEqual,
  )

  const [isSaved, setSaved] = useLocalStorage(`car-${stockNumber}`, false)

  const onFormSubmit = (e) => {
    e.preventDefault()
    setSaved(! isSaved)
  }

  return <ItemViewWrapper>
    <ItemViewImage />

    <ItemViewContainer>
      {
        ! item
          ? 'Loading...'
          : <>
              <ItemViewContent>
                <h1 style={{ font: Styles.Fonts.H1, marginBottom: '24px' }}>
                { item.manufacturerName } { item.modelName }
                </h1>

                <h4 style={{ font: Styles.Fonts.H4, marginBottom: '24px' }}>
                {[
                    `Stock #${item.stockNumber}`,
                    `${item.mileage.number} ${item.mileage.unit.toUpperCase()}`,
                    item.fuelType,
                    item.color,
                  ].join(' - ')}
                </h4>

                <p>
                  This car is currently available and can be delivered as soon as tomorrow morning. Please be aware that delivery times shown in this page are not definitive and may change due to bad weather conditions.
                </p>
              </ItemViewContent>

              <ItemViewSidebar>
                <Form onSubmit={onFormSubmit}>
                  <FormItem>
                    <p>If you like this car, click the button and save it in your collection of favourite items.</p>
                  </FormItem>
                  <FormFooter>
                    <Button type="submit">
                      { isSaved ? 'Remove' : 'Save' }
                    </Button>
                  </FormFooter>
                </Form>
              </ItemViewSidebar>
            </>
      }
    </ItemViewContainer>
  </ItemViewWrapper>
}

export default ItemView
