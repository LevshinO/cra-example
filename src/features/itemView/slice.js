import { createSlice } from '@reduxjs/toolkit'

import Api from '../../lib/Api'


const apiClient = new Api()

const initialState = {
  item: null,
}

const itemViewSlice = createSlice({
  name: 'itemView',
  initialState,
  reducers: {
    setFetchedData (state, { payload: { item } }) {
      state.item = item
    },
  },
})

export const {
  setFetchedData,
} = itemViewSlice.actions

export const defaultSelector = ({ itemView }) => itemView

export const fetchItem = (stockNumber) => async (dispatch) => {
  try {
    const { car } = await apiClient.getOneCar(stockNumber)

    dispatch(setFetchedData({ item: car }))
  } catch (error) {
    console.error(error)

    return error
  }

  return null
}

export default itemViewSlice.reducer
