import React from 'react'
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import Header from '../../layout/Header'
import Main from '../../layout/Main'
import Footer from '../../layout/Footer'

import HttpError from '../../components/HttpError'

import FilterForm from '../filterForm/FilterForm'
import ItemsList from '../itemsList/ItemsList'
import ItemView from '../itemView/ItemView'


const App = () => {
  return (
    <>
      <Header />

      <Main>
        <Switch>
          <Route exact path="/">
            <FilterForm />
            <ItemsList />
          </Route>

          <Route exact path="/cars/:stockNumber">
            <ItemView />
          </Route>

          <Route
            exact
            path="/error/:statusCode"
            render={(rProps) => <HttpError status={rProps.match.params.statusCode} />}
          />

          <Redirect from="*" to="/error/404" />
        </Switch>
      </Main>

      <Footer />
    </>
  );
}

export default App
