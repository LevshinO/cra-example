import React from 'react'
import styled from 'styled-components'

import { Styles } from '../../utils/constants'


const Header = styled.h3`
  font: ${Styles.Fonts.H3};
  margin-bottom: 24px;
`

const SubHeader = styled.h4`
  font: ${Styles.Fonts.H4};
  margin-bottom: 24px;
`

const ItemsListHeader = ({ showing = 0, results = 0 }) => (
  <>
    <Header>
      Available cars
    </Header>

    <SubHeader>
      Showing {showing} of {results} results
    </SubHeader>
  </>
)

export default ItemsListHeader
