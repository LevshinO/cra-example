import React from 'react'
import styled from 'styled-components'

import AddToFavorites from '../../components/AddToFavorites'
import Link from '../../components/Link'

import { Styles } from '../../utils/constants'


const ItemWrapper = styled.article`
  align-items: flex-start;
  border: 2px solid ${Styles.Colors.LightGray};
  display: flex;
  padding: 12px;
  position: relative;

  & + & {
    margin-top: 12px;
  }
`

const ItemPicture = styled.img`
  margin-right: 24px;
  width: 95px;
`

const ItemHeader = styled.h3`
  font: ${Styles.Fonts.H3};
  margin-bottom: 12px;
`

const ItemParams = styled.p`
  margin-bottom: 12px;
`

const AddToFavoritesWrapper = styled.span`
  position: absolute;
  right: 12px;
  top: 12px;
`

const Item = ({
  stockNumber,
  pictureUrl,
  manufacturerName,
  modelName,
  mileage,
  fuelType,
  color,
}) => {

  return <ItemWrapper>
    <ItemPicture
      alt=""
      src={pictureUrl}
    />

    <div>
      <ItemHeader>
        { manufacturerName } { modelName }
      </ItemHeader>

      <ItemParams>
        {[
          `Stock #${stockNumber}`,
          `${mileage.number} ${mileage.unit.toUpperCase()}`,
          fuelType,
          color,
        ].join(' - ')}
      </ItemParams>

      <Link to={`/cars/${stockNumber}`}>
        View details
      </Link>
    </div>

    <AddToFavoritesWrapper>
      <AddToFavorites storageId={`car-${stockNumber}`} />
    </AddToFavoritesWrapper>
  </ItemWrapper>
}

export default Item
