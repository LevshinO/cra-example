import { createSlice } from '@reduxjs/toolkit'

import Api from '../../lib/Api'


const apiClient = new Api()

const initialState = {
  items: [],
  totalPageCount: 0,
  totalItemsCount: 0,
}

const itemsListSlice = createSlice({
  name: 'itemsList',
  initialState,
  reducers: {
    setFetchedData (state, { payload: { items, totalPageCount, totalItemsCount } }) {
      state.items = items
      state.totalPageCount = totalPageCount
      state.totalItemsCount = totalItemsCount
    },
  },
})

export const { setFetchedData } = itemsListSlice.actions

export const defaultSelector = ({ itemsList }) => itemsList

export const fetchItems = ({ manufacturer = null, color = null, page = 1 }) => async (dispatch) => {
  try {
    const { cars, totalPageCount, totalCarsCount } = await apiClient.getAllCars({ manufacturer, color, page })

    dispatch(setFetchedData({ items: cars, totalPageCount, totalItemsCount: totalCarsCount }))
  } catch (error) {
    console.error(error)

    return error
  }

  return null
}

export default itemsListSlice.reducer
