import React, { useEffect } from 'react'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { useHistory } from "react-router-dom"
import { useQueryParams, StringParam, NumberParam, withDefault } from 'use-query-params'
import styled from 'styled-components'

import Item from './Item'
import ItemsListHeader from './ItemsListHeader'
import Pagination from '../../components/Pagination'

import {
  defaultSelector as itemsListSelector,
  fetchItems,
} from './slice'


const ItemsListWrapper = styled.section`
  flex-basis: 70%;
`

const ItemsList = ({
  items = [],
  page = 0,
  itemsPerPage = 10,
  totalItemsCount = 0,
}) => {

  const showing = itemsPerPage * page + items.length

  return ! items.length
    ? 'Loading...'
    : <ItemsListWrapper>
        <ItemsListHeader showing={showing} results={totalItemsCount} />

        { items.map((item, idx) => (
          <Item key={`car-${idx}`} {...item} />
        )) }

        <Pagination
          initialPage={page}
          initialPageSize={itemsPerPage}
          totalItems={totalItemsCount}
        />
      </ItemsListWrapper>
}

const ItemsListContainer = () => {
  const dispatch = useDispatch()

  const {
    items,
    totalItemsCount,
    itemsPerPage = 10,
  } = useSelector(
    itemsListSelector,
    shallowEqual,
  )

  let [{ color, manufacturer, page }] = useQueryParams({
    color: withDefault(StringParam, ''),
    manufacturer: withDefault(StringParam, ''),
    page: withDefault(NumberParam, 0),
  })

  page = page > 0 ? page - 1 : 0

  const history = useHistory()

  useEffect(
    () => {
      (async () => {
        const error = await dispatch(fetchItems({
          manufacturer,
          color,
          page: page + 1
        }))

        if (error?.payload?.status === 404) {
          history.replace('/error/404')
        }
      })()
    },
    [dispatch, manufacturer, color, page, history],
  )

  return <ItemsList
    items={items}
    page={page}
    itemsPerPage={itemsPerPage}
    totalItemsCount={totalItemsCount}
  />
}

export default ItemsListContainer
