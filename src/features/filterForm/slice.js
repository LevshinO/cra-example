import { createSlice } from '@reduxjs/toolkit'

import Api from '../../lib/Api'


const apiClient = new Api()

const INITIAL_COLORS = [{ label: 'All colors', value: '' }]
const INITIAL_MANUFACTURERS = [{ label: 'All manufacturers', value: '' }]

const initialState = {
  colors: INITIAL_COLORS,
  manufacturers: INITIAL_MANUFACTURERS,
}

const filterFormSlice = createSlice({
  name: 'filterForm',
  initialState,
  reducers: {
    setColors (state, { payload: colors }) {
      state.colors = colors
    },
    setManufacturers (state, { payload: manufacturers }) {
      state.manufacturers = manufacturers
    },
    setFilters () {
    },
  }
})

export const {
  setColors,
  setManufacturers,
  setFilters,
} = filterFormSlice.actions

export const defaultSelector = ({ filterForm }) => filterForm

export const fetchFilters = () => async (dispatch) => {
  try {
    const [{colors}, {manufacturers}] = await Promise.all([
      apiClient.getColors(),
      apiClient.getManufacturers(),
    ])

    const mappedColors = INITIAL_COLORS.concat(colors.map((color) => ({ label: color, value: color })))
    const mappedManufacturers = INITIAL_MANUFACTURERS.concat(manufacturers.map(({ name }) => ({ label: name, value: name })))

    dispatch(setColors(mappedColors))
    dispatch(setManufacturers(mappedManufacturers))
  } catch (error) {
    console.error(error)
  }
}

export default filterFormSlice.reducer
