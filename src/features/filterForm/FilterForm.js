import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch, shallowEqual } from 'react-redux'
import { useQueryParams, StringParam, withDefault } from 'use-query-params'
import styled from 'styled-components'

import Button from '../../components/Button'
import { Form, FormItem, FormFooter, Label } from '../../components/Form'
import Select from '../../components/Select'

import {
  setFilters,
  defaultSelector as filterFormSelector,
  fetchFilters,
} from './slice'


const AsideStyled = styled.aside`
  flex-basis: 30%;
  margin-right: 24px;
`

const FilterFormSelect = ({ label = '', options = [], ...props }) => (
  <FormItem>
    <Label htmlFor={props.id ?? ''}>{ label }</Label>
    <Select
      data-testid={props.id ?? ''}
      options={options}
      {...props}
    />
  </FormItem>
)

const FilterForm = () => {
  const dispatch = useDispatch()

  const {
    colors,
    manufacturers,
  } = useSelector(
    filterFormSelector,
    shallowEqual,
  )

  useEffect(() => { dispatch(fetchFilters()) }, [dispatch])

  const [{ color: defaultColor, manufacturer: defaultManufacturer }, setQueryParams] = useQueryParams({
    color: withDefault(StringParam, ''),
    manufacturer: withDefault(StringParam, ''),
  })

  const [color, setColor] = useState(defaultColor)
  const [manufacturer, setManufacturer] = useState(defaultManufacturer)

  const onFormSubmit = (e) => {
    e.preventDefault()
    dispatch(setFilters({ color, manufacturer }))
    setQueryParams({ color, manufacturer, page: 1 })
  }

  return <AsideStyled>
    <Form onSubmit={onFormSubmit}>
      <FilterFormSelect
        id="filter-color"
        label="Color"
        options={colors}
        value={color}
        onChange={(e) => setColor(e.target.value)}
      />
      <FilterFormSelect
        id="filter-manufacturer"
        label="Manufacturer"
        options={manufacturers}
        value={manufacturer}
        onChange={(e) => setManufacturer(e.target.value)}
      />
      <FormFooter>
        <Button type="submit">
          Filter
        </Button>
      </FormFooter>
    </Form>
  </AsideStyled>
}

export default FilterForm
