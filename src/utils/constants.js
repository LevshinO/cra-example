export const Styles = {
  Colors: {
    LightGray: '#EDEDED',
    DarkGray: '#4A4A4A',
    LightOrange: '#EA7F28',
    DarkOrange: '#D37324',
  },
  Fonts: {
    H1: 'bold 32px Roboto',
    H3: 'bold 18px Roboto',
    H4: '18px Roboto',
    Regular: '14px Roboto',
    Small: '12px Roboto',
  },
}

export const MenuLinks = [
  { name: 'Purchase', href: '/purchase', },
  { name: 'My orders', href: '/my-orders', },
  { name: 'Sell', href: '/sell', },
]
