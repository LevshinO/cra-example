import React from 'react'
import styled from 'styled-components'
import { getReasonPhrase } from 'http-status-codes'

import Link from './Link'
import Logo from './Logo'

import { Styles } from '../utils/constants'


const SectionStyled = styled.section`
  align-self: center;
  text-align: center;
  width: 100%;
`

const Title = styled.h1`
  font: ${Styles.Fonts.H1};
  margin-bottom: 24px;
`

const SubTitle = styled.h1`
  font: ${Styles.Fonts.H4};
  margin-bottom: 24px;
`

const HttpError = ({ status = 500 }) => {
  let statusText = 'Unknown Error'

  try {
    statusText = getReasonPhrase(status)
  } catch (error) {
  }

  return <SectionStyled>
    <Logo margin='0 auto 24px' />

    <Title>
      { status } &mdash; { statusText }
    </Title>
    <SubTitle>
      Sorry, something went wrong.
    </SubTitle>
    <SubTitle>
      You can always go back to the <Link to="/">homepage</Link>.
    </SubTitle>
  </SectionStyled>
}

export default HttpError
