import React, { useEffect } from 'react'
import styled, { css } from 'styled-components'
import { usePagination } from "react-use-pagination"
import { useQueryParam, NumberParam, withDefault } from 'use-query-params'

import { linkStyles } from './Link'


const PaginationWrapper = styled.div`
  margin-top: 24px;
  text-align: center;
  user-select: none;
`

const paginationItemStyles = css`
  margin-right: 24px;

  &:last-child {
    margin-right: 0;
  }
`

const PaginationItem = styled.span`
  ${paginationItemStyles}
`

const PaginationLink = styled.a`
  ${linkStyles}
  ${paginationItemStyles}
`

const Pagination = ({ initialPage = 0, initialPageSize = 10, totalItems = 0 }) => {
  const {
    currentPage,
    totalPages,
    nextEnabled,
    previousEnabled,
    setPage,
  } = usePagination({
    initialPage,
    initialPageSize,
    totalItems,
  })

  useEffect(() => setPage(initialPage), [setPage, initialPage])

  const actualCurrentPage = currentPage + 1
  const actualNextPage = currentPage + 2
  const isFirstDisabled = currentPage === 0
  const isLastDisabled = currentPage >= totalPages - 1

  const [, setPageQueryParam] = useQueryParam('page', withDefault(NumberParam, initialPage))

  const onPaginationClick = (page) => () => {
    setPage(page - 1)
    setPageQueryParam(page)
  }

  return <PaginationWrapper>
    <PaginationLink onClick={onPaginationClick(1)} disabled={isFirstDisabled}>First</PaginationLink>
    <PaginationLink onClick={onPaginationClick(currentPage)} disabled={! previousEnabled}>Previous</PaginationLink>
    <PaginationItem>
      Page {actualCurrentPage} of {totalPages}
    </PaginationItem>
    <PaginationLink onClick={onPaginationClick(actualNextPage)} disabled={! nextEnabled}>Next</PaginationLink>
    <PaginationLink onClick={onPaginationClick(totalPages)} disabled={isLastDisabled}>Last</PaginationLink>
  </PaginationWrapper>
}

export default Pagination
