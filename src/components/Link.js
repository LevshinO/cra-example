import styled, { css } from 'styled-components'
import { Link } from "react-router-dom"

import { Styles } from '../utils/constants'


export const linkStyles = css`
  color: ${Styles.Colors.LightOrange};
  cursor: pointer;
  text-decoration: none;

  &:hover {
    color: ${Styles.Colors.DarkOrange};
    text-decoration: underline;
  }

  &[disabled] {
    opacity: .5;
    pointer-events: none;
  }
`

const LinkStyled = styled(Link)`
  ${linkStyles}
`

export default LinkStyled
