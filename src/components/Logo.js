import React from 'react'

import Link from './Link'


const Logo = ({ margin = 'unset', width = 175, }) => (
  <Link
    data-testid="logo"
    style={{ display: 'inline-block', margin: margin }}
    to="/"
  >
    <img
      alt="Logo"
      style={{ width: `${width}px` }}
      src="https://auto1-js-task-api--mufasa71.repl.co/images/logo.png"
    />
  </Link>
)

export default Logo
