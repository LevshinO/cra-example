import React from 'react'

import useLocalStorage from '../hooks/useLocalStorage'

import Star from './Star'


const AddToFavorites = ({ storageId, isAddedDefault = false }) => {

  const [isAdded, setAdded] = useLocalStorage(storageId, isAddedDefault)

  const onAddedClick = () => setAdded(! isAdded)

  return <Star isActive={isAdded} onClick={onAddedClick} />
}

export default AddToFavorites
