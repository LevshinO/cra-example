import styled from 'styled-components'

import { Styles } from '../utils/constants'


const Button = styled.button`
  background-color: ${Styles.Colors.LightOrange};
  border: none;
  border-radius: 4px;
  color: #fff;
  cursor: pointer;
  height: 32px;
  outline: none;
  width: 128px;

  &:active {
    background-color: ${Styles.Colors.DarkOrange};
  }
`

export default Button
