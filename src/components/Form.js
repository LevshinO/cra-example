import styled from 'styled-components'

import { Styles } from '../utils/constants'


export const Form = styled.form`
  border: 2px solid ${Styles.Colors.LightGray};
  padding: 24px;
`

export const FormItem = styled.div`
  & + & {
    margin-top: 12px;
  }
`

export const FormFooter = styled.div`
  margin-top: 24px;
  text-align: right;
`

export const Label = styled.label`
  display: block;
  font: ${Styles.Fonts.Small};
  margin-bottom: 8px;
`
